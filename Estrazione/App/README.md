# Download zip files from minIO in Python using Boto3

## Getting Started

### Requirements

This project needs:
pipenv
```
pip install pipenv
```


### Setup and uploading
Use pull command to download the lastest MinIO docker image:
```
docker pull minio/minio
```

Create the container:
```
docker run -t -d -p 9000:9000 --name miniozf \
-e "MINIO_ACCESS_KEY=admin" \
-e "MINIO_SECRET_KEY=keystone" \
-v /home/usr/mdata:/data \
minio/minio server /data
```

There must be a bucket named "bucketzf" inside with a zip folder inside named "Compressa.zip. Inside the folder two files, 'testo.txt' and 'model.py'



Configure the environment :
```
pipenv shell
```

At your command line or shell, in the Estrazione/App directory, build the image with the following command:
```
docker build -f Dockerfile -t estrazione .
```

Run the image docker:
```
docker run --network=host -d estrazione
```

