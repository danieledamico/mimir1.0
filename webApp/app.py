from flask import Flask, render_template, redirect, request, url_for, session
from flask_mysqldb import MySQL, MySQLdb 
from flask_restful import Resource, Api
import bcrypt
import re

app = Flask(__name__)
api = Api(app)
app.secret_key = "^A&%12AJJOK)=123^?"

app.config['MYSQL_HOST'] = 'localhost'
app.config['MYSQL_USER'] = 'rickSanchez'
app.config['MYSQL_PASSWORD'] = 'password'
app.config['MYSQL_DB'] = 'flaskApp'
app.config['MYSQL_CURSORCLASS'] = 'DictCursor'
mysql = MySQL(app)


@app.route('/')
def home():
    return render_template("home.html")


@app.route('/register', methods=['GET', 'POST'])
def register():
    # Output message if something goes wrong...
    msg = ''
    # Check if "username", "password" and "email" POST requests exist (user submitted form)
    if request.method == 'POST' and 'username' in request.form and 'email' in request.form and 'password' in request.form:
        # Create variables for easy access
        username = request.form['username']
        email = request.form['email']
        password = request.form['password']
        # Check if account exists using MySQL
        cursor = mysql.connection.cursor(MySQLdb.cursors.DictCursor)
        cursor.execute('SELECT * FROM users WHERE username = %s', (username,))
        account = cursor.fetchone()
        # If account exists show error and validation checks
        if account:
            session['username'] = account['username']
            session['email'] = account['email']
            session['password'] = account['password']
            msg = 'Account already exists!'
        elif not username or not password or not email:
            msg = 'Please fill out the form!'
        elif not re.match(r'[A-Za-z0-9]+', username):
            msg = 'Username must contain only characters and numbers!'
        elif not re.match(r'[^@]+@[^@]+\.[^@]+', email):
            msg = 'Invalid email address!'
        else:
            # Account doesnt exists and the form data is valid, now insert new account into accounts table
            cursor.execute('INSERT INTO users VALUES (NULL, %s, %s, %s)', (username, email, password,))
            mysql.connection.commit()
            return render_template(url_for('home'))
    elif request.method == 'POST':
        # Form is empty... (no POST data)
        msg = 'Please fill out the form!'
    # Show registration form with message (if any)
    return render_template("register.html", msg=msg)



@app.route('/login', methods=['GET', 'POST'])
def login():
    msg=''
    if request.method == 'POST' and 'username' in request.form and 'password' in request.form:
        # Create variables for easy access
        username = request.form['username']
        password = request.form['password']
        # Check if account exists using MySQL
        cursor = mysql.connection.cursor(MySQLdb.cursors.DictCursor)
        cursor.execute('SELECT * FROM users WHERE username = %s AND password = %s', (username, password,))
        # Fetch one record and return result
        account = cursor.fetchone()
        # If account exists in accounts table in out database
        if account:
            # Create session data, we can access this data in other routes
            session['loggedin'] = True
            session['id'] = account['id']
            session['username'] = account['username']
            # Redirect to home page
            return redirect(url_for('home'))
        else:
            # Account doesnt exist or username/password incorrect
            msg = 'Incorrect username/password!'
    return render_template("login.html", msg=msg)



@app.route('/logout', methods=["GET", "POST"])
def logout():
    session.clear()
    return render_template("home.html")


@app.route('/newNotebook')
def newNotebook():
    return render_template("newNotebook.html")


@app.route('/notebookList')
def notebookList():
	return render_template("notebookList.html")


@app.route('/newEndpoint')
def newEndpoint():
    return render_template("newEndpoint.html")


@app.route('/endpointList')
def endpointList():
	return render_template("endpointList.html")


@app.route('/newTraining')
def newTraining():
	return render_template("newTraining.html")


@app.route('/trainingList')
def trainingList():
	return render_template("trainingList.html")


@app.route('/registerError')
def registerError():
	return render_template("registerError.html")


if __name__ == '__main__':

	app.run(debug=True)