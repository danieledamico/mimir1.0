from datetime import datetime
from flask import make_response, abort
import uuid

def get_timestamp():
    return datetime.now().strftime(("%Y-%m-%d %H:%M:%S"))

# Data to serve with our API
ENDPOINTS = {}


def getAllEndpoint():
    """
    :return:        sorted list of endpoint
    """
    # Create the list of endpoint from our data
    return [ENDPOINTS[key] for key in sorted(ENDPOINTS.keys())] 


def getEndpointById(endpoint_id):
    """
    :param endpoint_id:  id of endpoint to find
    :return:       endpoint matching by name
    """
    if endpoint_id in ENDPOINTS:
        endpoint = ENDPOINTS.get(endpoint_id)

    else:
        abort(
            404, "endpoint with this id= {endpoint_id} not found".format(endpoint_id=endpoint_id)
        )

    return endpoint


def newEndpoint(endpoint):

	name = endpoint.get("endpoint_name", None)
	id = str(uuid.uuid4())

	if id not in ENDPOINTS and id is not None:
		ENDPOINTS[id]= {
			"endpoint_name": name, 
			"endpoint_id": id,
			"createdDate": get_timestamp(),
		}
      
		return endpoint
		
	else:
		abort(406,"endpoint with this id : {endpoint_id} already exist".format(endpoint_id=endpoint_id),)

	




def updateEndpoint(endpoint_id,endpoint):
    """
    :param endpoint_id:   id of endpoint to update in the endpoint structure
    :param endpoint:  endpoint to update
    :return:        updated endpoint structure
    """
    
    if endpoint_id in ENDPOINTS:
        ENDPOINTS[endpoint_id]["endpoint_name"] = endpoint.get("endpoint_name")
        return ENDPOINTS[endpoint_id]


    else:
        abort(
            404, "endpoint with this id {endpoint_id} not found".format(endpoint_id=endpoint_id)
        )



def deleteEndpoint(endpoint_id):
    """
    :param endpoint_id:   id of endpoint to delete
    :return:        200 on successful delete, 404 if not found
    """

    if endpoint_id in ENDPOINTS:
        del ENDPOINTS[endpoint_id]
        return make_response(
            "{endpoint_id} successfully deleted".format(endpoint_id=endpoint_id), 200
        )

    else:
        abort(
            404, "training with this id: {endpoint_id} not found".format(endpoint_id=endpoint_id)
        )

