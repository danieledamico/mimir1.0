from datetime import datetime
from flask import make_response, abort
import uuid

def get_timestamp():
    return datetime.now().strftime(("%Y-%m-%d %H:%M:%S"))

# Data to serve with our API
NOTEBOOKS = {}


def getAllNotebook():
    """
    :return:        sorted list of notebook
    """
    # Create the list of notebook from our data
    return [NOTEBOOKS[key] for key in sorted(NOTEBOOKS.keys())] 


def getNotebookById(notebook_id):
    """
    :param notebook_id:  id of notebook to find
    :return:       notebook matching by name
    """
    if notebook_id in NOTEBOOKS:
        notebook = NOTEBOOKS.get(notebook_id)

    else:
        abort(
            404, "Notebook with this id= {notebook_id} not found".format(notebook_id=notebook_id)
        )

    return notebook


def newNotebook(notebook):

	name = notebook.get("notebook_name", None)
	id = str(uuid.uuid4())

	if id not in NOTEBOOKS and id is not None:
		NOTEBOOKS[id]= {
			"notebook_name": name, 
			"notebook_id": id,
			"createdDate": get_timestamp(),
		}
      
		return notebook
		
	else:
		abort(406,"Notebook with this id : {notebook_id} already exist".format(notebook_id=notebook_id),)

	




def updateNotebook(notebook_id,notebook):
    """
    :param notebook_id:   id of notebook to update in the notebook structure
    :param notebook:  notebook to update
    :return:        updated notebook structure
    """
    
    if notebook_id in NOTEBOOKS:
        NOTEBOOKS[notebook_id]["notebook_name"] = notebook.get("notebook_name")
        return NOTEBOOKS[notebook_id]


    else:
        abort(
            404, "Notebook with this id {notebook_id} not found".format(notebook_id=notebook_id)
        )



def deleteNotebook(notebook_id):
    """
    :param notebook_id:   id of notebook to delete
    :return:        200 on successful delete, 404 if not found
    """

    if notebook_id in NOTEBOOKS:
        del NOTEBOOKS[notebook_id]
        return make_response(
            "{notebook_id} successfully deleted".format(notebook_id=notebook_id), 200
        )

    else:
        abort(
            404, "Notebook with this id: {notebook_id} not found".format(notebook_id=notebook_id)
        )
