/*
 * JavaScript file for the application to demonstrate
 * using the API
 */

// Create the namespace instance
let ns = {};

// Create the model instance
ns.model = (function() {
    'use strict';

    let $event_pump = $('body');

    // Return the API
    return {
        'read': function() {
            let ajax_options = {
                type: 'GET',
                url: 'api/notebook',
                accepts: 'application/json',
                dataType: 'json'
            };
            $.ajax(ajax_options)
            .done(function(data) {
                $event_pump.trigger('model_read_success', [data]);
            })
            .fail(function(xhr, textStatus, errorThrown) {
                $event_pump.trigger('model_error', [xhr, textStatus, errorThrown]);
            })
        },
        create: function(notebook_name, notebook_id) {
            let ajax_options = {
                type: 'POST',
                url: 'api/notebook',
                accepts: 'application/json',
                contentType: 'application/json',
                dataType: 'json',
                data: JSON.stringify({
                    'notebook_name': notebook_name,
                    'notebook_id': notebook_id
                })
            };
            $.ajax(ajax_options)
            .done(function(data) {
                $event_pump.trigger('model_create_success', [data]);
            })
            .fail(function(xhr, textStatus, errorThrown) {
                $event_pump.trigger('model_error', [xhr, textStatus, errorThrown]);
            })
        },
        update: function(notebook_name, notebook_id) {
            let ajax_options = {
                type: 'PUT',
                url: 'api/notebook/' + notebook_name,
                accepts: 'application/json',
                contentType: 'application/json',
                dataType: 'json',
                data: JSON.stringify({
                    'notebook_name': notebook_name,
                    'notebook_id': notebook_id
                })
            };
            $.ajax(ajax_options)
            .done(function(data) {
                $event_pump.trigger('model_update_success', [data]);
            })
            .fail(function(xhr, textStatus, errorThrown) {
                $event_pump.trigger('model_error', [xhr, textStatus, errorThrown]);
            })
        },
        'delete': function(notebook_id) {
            let ajax_options = {
                type: 'DELETE',
                url: 'api/notebook/' + notebook_id,
                accepts: 'application/json',
                contentType: 'plain/text'
            };
            $.ajax(ajax_options)
            .done(function(data) {
                $event_pump.trigger('model_delete_success', [data]);
            })
            .fail(function(xhr, textStatus, errorThrown) {
                $event_pump.trigger('model_error', [xhr, textStatus, errorThrown]);
            })
        }
    };
}());

// Create the view instance
ns.view = (function() {
    'use strict';

    let $notebook_name = $('#notebook_name'),
        $notebook_id = $('#notebook_id');

    // return the API
    return {
        reset: function() {
            $notebook_id.val('');
            $notebook_name.val('').focus();
        },
        update_editor: function(notebook_name, notebook_id) {
            $notebook_id.val(notebook_id);
            $notebook_name.val(notebook_name).focus();
        },
        build_table: function(notebook) {
            let rows = ''

            // clear the table
            $('.notebook table > tbody').empty();

            // did we get a notebook array?
            if (notebook) {
                for (let i=0, l=notebook.length; i < l; i++) {
                    rows += `<tr><td class="notebook_id">${notebook[i].notebook_id}</td><td class="notebook_name">${notebook[i].notebook_name}</td><td>${notebook[i].createdDate}</td></tr>`;
                }
                $('table > tbody').append(rows);
            }
        },
        error: function(error_msg) {
            $('.error')
                .text(error_msg)
                .css('visibility', 'visible');
            setTimeout(function() {
                $('.error').css('visibility', 'hidden');
            }, 3000)
        }
    };
}());

// Create the controller
ns.controller = (function(m, v) {
    'use strict';

    let model = m,
        view = v,
        $event_pump = $('body'),
        $notebook_name = $('#notebook_name'),
        $notebook_id = $('#notebook_id');

    // Get the data from the model after the controller is done initializing
    setTimeout(function() {
        model.read();
    }, 100)

    // Validate input
    function validate(notebook_name, notebook_id) {
        return notebook_name !== "" && notebook_id !== "";
    }

    // Create our event handlers
    $('#create').click(function(e) {
        let notebook_name = $notebook_name.val(),
            notebook_id = $notebook_id.val();

        e.preventDefault();

        if (validate(notebook_name, notebook_id)) {
            model.create(notebook_name, notebook_id)
        } else {
            alert('Problem with name');
        }
    });

    $('#update').click(function(e) {
        let notebook_name = $notebook_name.val(),
            notebook_id = $notebook_id.val();

        e.preventDefault();

        if (validate(notebook_name, notebook_id)) {
            model.update(notebook_name, notebook_id)
        } else {
            alert('Problem with name and id');
        }
        e.preventDefault();
    });

    $('#delete').click(function(e) {
        let notebook_id = $notebook_id.val();

        e.preventDefault();

        if (validate('placeholder', notebook_id)) {
            model.delete(notebook_id)
        } else {
            alert('Problem with id selection');
        }
        e.preventDefault();
    });

    $('#reset').click(function() {
        view.reset();
    })

    $('table > tbody').on('dblclick', 'tr', function(e) {
        let $target = $(e.target),
            notebook_name,
            notebook_id;

        notebook_name = $target
            .parent()
            .find('td.notebook_name')
            .text();

        notebook_id = $target
            .parent()
            .find('td.notebook_id')
            .text();

        view.update_editor(notebook_name, notebook_id);
    });

    // Handle the model events
    $event_pump.on('model_read_success', function(e, data) {
        view.build_table(data);
        view.reset();
    });

    $event_pump.on('model_create_success', function(e, data) {
        model.read();
    });

    $event_pump.on('model_update_success', function(e, data) {
        model.read();
    });

    $event_pump.on('model_delete_success', function(e, data) {
        model.read();
    });

    $event_pump.on('model_error', function(e, xhr, textStatus, errorThrown) {
        let error_msg = textStatus + ': ' + errorThrown + ' - ' + xhr.responseJSON.detail;
        view.error(error_msg);
        console.log(error_msg);
    })
}(ns.model, ns.view));

