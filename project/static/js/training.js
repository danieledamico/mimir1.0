/*
 * JavaScript file for the application to demonstrate
 * using the API
 */

// Create the namespace instance
let ns = {};

// Create the model instance
ns.model = (function() {
    'use strict';

    let $event_pump = $('body');

    // Return the API
    return {
        'read': function() {
            let ajax_options = {
                type: 'GET',
                url: 'api/training',
                accepts: 'application/json',
                dataType: 'json'
            };
            $.ajax(ajax_options)
            .done(function(data) {
                $event_pump.trigger('model_read_success', [data]);
            })
            .fail(function(xhr, textStatus, errorThrown) {
                $event_pump.trigger('model_error', [xhr, textStatus, errorThrown]);
            })
        },
        create: function(training_name, training_id) {
            let ajax_options = {
                type: 'POST',
                url: 'api/training',
                accepts: 'application/json',
                contentType: 'application/json',
                dataType: 'json',
                data: JSON.stringify({
                    'training_name': training_name,
                    'training_id': training_id
                })
            };
            $.ajax(ajax_options)
            .done(function(data) {
                $event_pump.trigger('model_create_success', [data]);
            })
            .fail(function(xhr, textStatus, errorThrown) {
                $event_pump.trigger('model_error', [xhr, textStatus, errorThrown]);
            })
        },
        update: function(training_name, training_id) {
            let ajax_options = {
                type: 'PUT',
                url: 'api/training/' + training_name,
                accepts: 'application/json',
                contentType: 'application/json',
                dataType: 'json',
                data: JSON.stringify({
                    'training_name': training_name,
                    'training_id': training_id
                })
            };
            $.ajax(ajax_options)
            .done(function(data) {
                $event_pump.trigger('model_update_success', [data]);
            })
            .fail(function(xhr, textStatus, errorThrown) {
                $event_pump.trigger('model_error', [xhr, textStatus, errorThrown]);
            })
        },
        'delete': function(training_id) {
            let ajax_options = {
                type: 'DELETE',
                url: 'api/training/' + training_id,
                accepts: 'application/json',
                contentType: 'plain/text'
            };
            $.ajax(ajax_options)
            .done(function(data) {
                $event_pump.trigger('model_delete_success', [data]);
            })
            .fail(function(xhr, textStatus, errorThrown) {
                $event_pump.trigger('model_error', [xhr, textStatus, errorThrown]);
            })
        }
    };
}());

// Create the view instance
ns.view = (function() {
    'use strict';

    let $training_name = $('#training_name'),
        $training_id = $('#training_id');

    // return the API
    return {
        reset: function() {
            $training_id.val('');
            $training_name.val('').focus();
        },
        update_editor: function(training_name, training_id) {
            $training_id.val(training_id);
            $training_name.val(training_name).focus();
        },
        build_table: function(training) {
            let rows = ''

            // clear the table
            $('.training table > tbody').empty();

            // did we get a training array?
            if (training) {
                for (let i=0, l=training.length; i < l; i++) {
                    rows += `<tr><td class="training_id">${training[i].training_id}</td><td class="training_name">${training[i].training_name}</td><td>${training[i].createdDate}</td></tr>`;
                }
                $('table > tbody').append(rows);
            }
        },
        error: function(error_msg) {
            $('.error')
                .text(error_msg)
                .css('visibility', 'visible');
            setTimeout(function() {
                $('.error').css('visibility', 'hidden');
            }, 3000)
        }
    };
}());

// Create the controller
ns.controller = (function(m, v) {
    'use strict';

    let model = m,
        view = v,
        $event_pump = $('body'),
        $training_name = $('#training_name'),
        $training_id = $('#training_id');

    // Get the data from the model after the controller is done initializing
    setTimeout(function() {
        model.read();
    }, 100)

    // Validate input
    function validate(training_name, training_id) {
        return training_name !== "" && training_id !== "";
    }

    // Create our event handlers
    $('#create').click(function(e) {
        let training_name = $training_name.val(),
            training_id = $training_id.val();

        e.preventDefault();

        if (validate(training_name, training_id)) {
            model.create(training_name, training_id)
        } else {
            alert('Problem with name');
        }
    });

    $('#update').click(function(e) {
        let training_name = $training_name.val(),
            training_id = $training_id.val();

        e.preventDefault();

        if (validate(training_name, training_id)) {
            model.update(training_name, training_id)
        } else {
            alert('Problem with name and id');
        }
        e.preventDefault();
    });

    $('#delete').click(function(e) {
        let training_id = $training_id.val();

        e.preventDefault();

        if (validate('placeholder', training_id)) {
            model.delete(training_id)
        } else {
            alert('Problem with id selection');
        }
        e.preventDefault();
    });

    $('#reset').click(function() {
        view.reset();
    })

    $('table > tbody').on('dblclick', 'tr', function(e) {
        let $target = $(e.target),
            training_name,
            training_id;

        training_name = $target
            .parent()
            .find('td.training_name')
            .text();

        training_id = $target
            .parent()
            .find('td.training_id')
            .text();

        view.update_editor(training_name, training_id);
    });

    // Handle the model events
    $event_pump.on('model_read_success', function(e, data) {
        view.build_table(data);
        view.reset();
    });

    $event_pump.on('model_create_success', function(e, data) {
        model.read();
    });

    $event_pump.on('model_update_success', function(e, data) {
        model.read();
    });

    $event_pump.on('model_delete_success', function(e, data) {
        model.read();
    });

    $event_pump.on('model_error', function(e, xhr, textStatus, errorThrown) {
        let error_msg = textStatus + ': ' + errorThrown + ' - ' + xhr.responseJSON.detail;
        view.error(error_msg);
        console.log(error_msg);
    })
}(ns.model, ns.view));
