/*
 * JavaScript file for the application to demonstrate
 * using the API
 */

// Create the namespace instance
let ns = {};

// Create the model instance
ns.model = (function() {
    'use strict';

    let $event_pump = $('body');

    // Return the API
    return {
        'read': function() {
            let ajax_options = {
                type: 'GET',
                url: 'api/endpoint',
                accepts: 'application/json',
                dataType: 'json'
            };
            $.ajax(ajax_options)
            .done(function(data) {
                $event_pump.trigger('model_read_success', [data]);
            })
            .fail(function(xhr, textStatus, errorThrown) {
                $event_pump.trigger('model_error', [xhr, textStatus, errorThrown]);
            })
        },
        create: function(endpoint_name, endpoint_id) {
            let ajax_options = {
                type: 'POST',
                url: 'api/endpoint',
                accepts: 'application/json',
                contentType: 'application/json',
                dataType: 'json',
                data: JSON.stringify({
                    'endpoint_name': endpoint_name,
                    'endpoint_id': endpoint_id
                })
            };
            $.ajax(ajax_options)
            .done(function(data) {
                $event_pump.trigger('model_create_success', [data]);
            })
            .fail(function(xhr, textStatus, errorThrown) {
                $event_pump.trigger('model_error', [xhr, textStatus, errorThrown]);
            })
        },
        update: function(endpoint_name, endpoint_id) {
            let ajax_options = {
                type: 'PUT',
                url: 'api/endpoint/' + endpoint_name,
                accepts: 'application/json',
                contentType: 'application/json',
                dataType: 'json',
                data: JSON.stringify({
                    'endpoint_name': endpoint_name,
                    'endpoint_id': endpoint_id
                })
            };
            $.ajax(ajax_options)
            .done(function(data) {
                $event_pump.trigger('model_update_success', [data]);
            })
            .fail(function(xhr, textStatus, errorThrown) {
                $event_pump.trigger('model_error', [xhr, textStatus, errorThrown]);
            })
        },
        'delete': function(endpoint_id) {
            let ajax_options = {
                type: 'DELETE',
                url: 'api/endpoint/' + endpoint_id,
                accepts: 'application/json',
                contentType: 'plain/text'
            };
            $.ajax(ajax_options)
            .done(function(data) {
                $event_pump.trigger('model_delete_success', [data]);
            })
            .fail(function(xhr, textStatus, errorThrown) {
                $event_pump.trigger('model_error', [xhr, textStatus, errorThrown]);
            })
        }
    };
}());

// Create the view instance
ns.view = (function() {
    'use strict';

    let $endpoint_name = $('#endpoint_name'),
        $endpoint_id = $('#endpoint_id');

    // return the API
    return {
        reset: function() {
            $endpoint_id.val('');
            $endpoint_name.val('').focus();
        },
        update_editor: function(endpoint_name, endpoint_id) {
            $endpoint_id.val(endpoint_id);
            $endpoint_name.val(endpoint_name).focus();
        },
        build_table: function(endpoint) {
            let rows = ''

            // clear the table
            $('.endpoint table > tbody').empty();

            // did we get a endpoint array?
            if (endpoint) {
                for (let i=0, l=endpoint.length; i < l; i++) {
                    rows += `<tr><td class="endpoint_id">${endpoint[i].endpoint_id}</td><td class="endpoint_name">${endpoint[i].endpoint_name}</td><td>${endpoint[i].createdDate}</td></tr>`;
                }
                $('table > tbody').append(rows);
            }
        },
        error: function(error_msg) {
            $('.error')
                .text(error_msg)
                .css('visibility', 'visible');
            setTimeout(function() {
                $('.error').css('visibility', 'hidden');
            }, 3000)
        }
    };
}());

// Create the controller
ns.controller = (function(m, v) {
    'use strict';

    let model = m,
        view = v,
        $event_pump = $('body'),
        $endpoint_name = $('#endpoint_name'),
        $endpoint_id = $('#endpoint_id');

    // Get the data from the model after the controller is done initializing
    setTimeout(function() {
        model.read();
    }, 100)

    // Validate input
    function validate(endpoint_name, endpoint_id) {
        return endpoint_name !== "" && endpoint_id !== "";
    }

    // Create our event handlers
    $('#create').click(function(e) {
        let endpoint_name = $endpoint_name.val(),
            endpoint_id = $endpoint_id.val();

        e.preventDefault();

        if (validate(endpoint_name, endpoint_id)) {
            model.create(endpoint_name, endpoint_id)
        } else {
            alert('Problem with name');
        }
    });

    $('#update').click(function(e) {
        let endpoint_name = $endpoint_name.val(),
            endpoint_id = $endpoint_id.val();

        e.preventDefault();

        if (validate(endpoint_name, endpoint_id)) {
            model.update(endpoint_name, endpoint_id)
        } else {
            alert('Problem with name and id');
        }
        e.preventDefault();
    });

    $('#delete').click(function(e) {
        let endpoint_id = $endpoint_id.val();

        e.preventDefault();

        if (validate('placeholder', endpoint_id)) {
            model.delete(endpoint_id)
        } else {
            alert('Problem with id selection');
        }
        e.preventDefault();
    });

    $('#reset').click(function() {
        view.reset();
    })

    $('table > tbody').on('dblclick', 'tr', function(e) {
        let $target = $(e.target),
            endpoint_name,
            endpoint_id;

        endpoint_name = $target
            .parent()
            .find('td.endpoint_name')
            .text();

        endpoint_id = $target
            .parent()
            .find('td.endpoint_id')
            .text();

        view.update_editor(endpoint_name, endpoint_id);
    });

    // Handle the model events
    $event_pump.on('model_read_success', function(e, data) {
        view.build_table(data);
        view.reset();
    });

    $event_pump.on('model_create_success', function(e, data) {
        model.read();
    });

    $event_pump.on('model_update_success', function(e, data) {
        model.read();
    });

    $event_pump.on('model_delete_success', function(e, data) {
        model.read();
    });

    $event_pump.on('model_error', function(e, xhr, textStatus, errorThrown) {
        let error_msg = textStatus + ': ' + errorThrown + ' - ' + xhr.responseJSON.detail;
        view.error(error_msg);
        console.log(error_msg);
    })
}(ns.model, ns.view));