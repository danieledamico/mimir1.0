from datetime import datetime
from flask import make_response, abort
import uuid

def get_timestamp():
    return datetime.now().strftime(("%Y-%m-%d %H:%M:%S"))

# Data to serve with our API
TRAININGS = {}


def getAllTraining():
    """
    :return:        sorted list of training
    """
    # Create the list of training from our data
    return [TRAININGS[key] for key in sorted(TRAININGS.keys())] 


def getTrainingById(training_id):
    """
    :param training_id:  id of training to find
    :return:       training matching by name
    """
    if training_id in TRAININGS:
        training = TRAININGS.get(training_id)

    else:
        abort(
            404, "training with this id= {training_id} not found".format(training_id=training_id)
        )

    return training


def newTraining(training):

	name = training.get("training_name", None)
	id = str(uuid.uuid4())

	if id not in TRAININGS and id is not None:
		TRAININGS[id]= {
			"training_name": name, 
			"training_id": id,
			"createdDate": get_timestamp(),
		}
      
		return training
		
	else:
		abort(406,"training with this id : {training_id} already exist".format(training_id=training_id),)

	




def updateTraining(training_id,training):
    """
    :param training_id:   id of training to update in the training structure
    :param training:  training to update
    :return:        updated training structure
    """
    
    if training_id in TRAININGS:
        TRAININGS[training_id]["training_name"] = training.get("training_name")
        return TRAININGS[training_id]


    else:
        abort(
            404, "training with this id {training_id} not found".format(training_id=training_id)
        )



def deleteTraining(training_id):
    """
    :param training_id:   id of training to delete
    :return:        200 on successful delete, 404 if not found
    """

    if training_id in TRAININGS:
        del TRAININGS[training_id]
        return make_response(
            "{training_id} successfully deleted".format(training_id=training_id), 200
        )

    else:
        abort(
            404, "training with this id: {training_id} not found".format(training_id=training_id)
        )
