from flask import Flask, render_template
import connexion

app = connexion.App(__name__,specification_dir='./')
app.add_api('openapi.yml')


@app.route('/')
def notebook():
    return render_template('notebook.html')


@app.route('/training')
def training():
    return render_template('training.html')


@app.route('/endpoint')
def endpoint():
    return render_template('endpoint.html')

if __name__ == '__main__':
    app.run(host='0.0.0.0',port=5000,debug=True)

